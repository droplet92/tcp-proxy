package main

import (
	"fmt"
	"io"
	"net"
)

func main() {
	laddr, err := net.ResolveTCPAddr("tcp", ":8001")
	if err != nil {
		panic(err)
	}

	listener, err := net.ListenTCP("tcp", laddr)
	if err != nil {
		panic(err)
	}

	for {
		conn, err := listener.AcceptTCP()
		if err != nil {
			fmt.Println("Accept Error:", err)
			continue
		}
		defer conn.Close()

		copyConn(conn)
	}
}

func copyConn(src *net.TCPConn) {
	raddr, err := net.ResolveTCPAddr("tcp", ":8002")
	if err != nil {
		panic(err)
	}

	dst, err := net.DialTCP("tcp", nil, raddr)
	if err != nil {
		panic(err)
	}
	defer dst.Close()

	done := make(chan bool)

	go func() {
		io.Copy(dst, src)
		done <- true
	}()

	go func() {
		io.Copy(src, dst)
		done <- true
	}()

	<-done
	<-done
}
